
fennel = require("fennel")

redbean_major = (GetRedbeanVersion() & 0xFF0000) >> 16
redbean_minor = (GetRedbeanVersion() & 0x00FF00) >> 8
redbean_patch = (GetRedbeanVersion() & 0x0000FF)

print("Starting Redbean-Fennel REPL")
print("Redbean " .. redbean_major .. "." .. redbean_minor .. "." .. redbean_patch .. "; Fennel " .. fennel.version)
print("See https://redbean.dev/#hooks for hooks and functions.")

fennel:repl()
