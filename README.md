
Template project for creating a Redbean server with Fennel support. This means
you get a run-anywhere Lisp server with good performance in just a few megabytes.

Let's break these features down:

* 'run-anywhere' Thanks to Actually Portable Executables, the binary runs natively on Windows, BSD, macOS, and Linux on x86_64, and via qemu on other platforms.
* 'Lisp' Fennel is a Lisp with Clojure-inspired syntax, pattern matching, and macro support that compiles to Lua.
* 'good performance' The server can handle tens of thousands of requests in my testing on an two-core Core i5 and thousands of requests per second on a low-end vps. A Core i9 eight-core processor could likely handle hundreds of thousands of requests.
* 'just a few megabytes' The base redbean image is about 1.6 MB and adding Fennel is neglible. Docker images are a similar size.

This template isn't ready for serious use without extensive testing. Redbean and Fennel are
very stable in my experience, but they're relatively young, and I'm not aware of anyone else using them together.

* Issues: At [GitLab](https://gitlab.com/actuallyalys/redbean-fennel/-/issues/new) or via [email](mailto:incoming+actuallyalys-redbean-fennel-28364647-issue-@incoming.gitlab.com)
* [Official mercurial repository](https://rhodecode.alysbrooks.com/alysbrooks-public/redbean-fennel)
* [GitLab mirror](https://gitlab.com/actuallyalys/redbean-fennel)

## Usage ##

This template creates binaries that can be run anywhere, but it requires InfoZip
and `make`, so you need to install these utilities on Windows (perhaps
via Cygwin or WSL).

1. Either clone the repository or download a zip file to a directory of your
   choice.
2. Change `output_file` in `Makefile` to your desired app name.
3. Add Fennel, Lua, and HTML files to the `src/` directory.
4. Run `make debug` to create the file.
4. Run `make run` to run it.

### Deployment ###

Running `make release` creates a release binary. You can transfer it to a server
and start it as a service.

Alternatively, you can build a Docker image with `make docker`. 
While a Docker image is arguably overkill when `make release` creates a standalone,
multiplatform binary, you might want to use the Docker image for consistency
with your other services, for added security, or because you use a tool like Kubernetes.

That being said, the Docker images are very small and on a Linux system, Docker has
[minimal overhead](https://stackoverflow.com/a/26149994), so while it's
technically more complex, the cost is very slight.

For more details, read the [ops](https://redbean.dev/#ops) section of the
Redbean documentation.

### REPL ###

You can run a REPL:

    make repl

### Upgrades ###

You can upgrade Fennel and Redbean by modifying the `version` and
`fennel-version` variables in the Makefile.

There are sometimes improvements in `Makefile` and `src/.init.lua`. This
template is less than 100 lines in total so these updates are hopefully
manageable.

#### Redbean 1.5.0 ####

Replace `zip:` to `/zip/`, including in `src/.init.lua`. You can also download an
updated version of the file instead.


#### Redbean 2.0.0 (template 0.1.38) ####

See [the Cosmopolitan 2.0 release notes](https://github.com/jart/cosmopolitan/releases/tag/2.0)
for additional breaking changes.

## Troubleshooting ##

### `run-detectors: unable to find an interpreter` ###

According to the creator of Redbean, this happens if you have binfmt_misc enabled. Run:

    sudo sh -c "echo ':APE:M::MZqFpD::/bin/sh:' >/proc/sys/fs/binfmt_misc/register"


### `invalid file (bad magic number)` ###

I'm honestly not sure why this happens. It may be that certain errors leave the
zip file in an incorrect state. But I do know how to fix it: Run `make clean` to
delete both the dependencies and the build artifacts. Note that this will delete
any files you manually added to the redbean `.com` file and your generated
`.com` file.


### Segfault (recent Linux kernels) ###

Segfaults can of course have many causes. A [common cause of
segfaults](https://github.com/jart/cosmopolitan/issues/344) occurs on Linux.
When stracing, `redbean` opens itself before performing two `mmap` calls. It appears to be a
problem on certain Linux kernels. I and other bug reporters encountered the problem on 5.15 while 5.14
appears to work fine.

Upgrading to redbean 2.0.0 or higher appears to fix the issue. 

## Acknowledgements ##

Thanks to [Justine Tunney](https://justine.lol/) for Redbean and Actually
Portable Executables, [György Kiss](https://github.com/kissgyorgy) for the
Redbean Docker file, and [Calvin Rose](https://github.com/bakpakin/tiny-ecs) for
Fennel.

## License

(c) 2023 Alys S Brooks

Licensed under the MIT license, same as Fennel and similar to Redbean (it uses
the ISC, which is nigh identical to the MIT license).
