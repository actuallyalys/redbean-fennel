#Thanks to kissgyorgy
#This is patterned after redbean-docker: https://github.com/kissgyorgy/redbean-docker/blob/34a1c7363becaf73ad160547f8bf338589b269c6/Dockerfile-multistage

FROM alpine:latest as build

ARG OUT=out

RUN apk add --update zip bash

COPY ./${OUT}-prod.com /out-prod.com
RUN sh /out-prod.com --assimilate

#For debugging
RUN zip -sf /out-prod.com

FROM scratch

COPY --from=build ./out-prod.com /

# You probably only need to modify this part of the file:
CMD ["/out-prod.com", "-vv", "-p", "80"]
